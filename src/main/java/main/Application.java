package main;

import main.dao.IDAODisc;
import main.domain.Disc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
	
	@Autowired
	private IDAODisc daoDisc;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public String initRepo() {
		this.daoDisc.save(new Disc(new Long("1"), "Stadium Arcadium", "Red Hot Chili Peppers", "rhcp", "", "5th album from the band"));
		this.daoDisc.save(new Disc(new Long("2"), "Californication", "Red Hot Chili Peppers", "rhcp", "", "3rd album from the band"));
		this.daoDisc.save(new Disc(new Long("3"), "By The Way", "Red Hot Chili Peppers", "rhcp", "", "4th album from the band"));
		this.daoDisc.save(new Disc(new Long("4"), "True Men Don't Kill Coyotes", "Red Hot Chili Peppers", "rhcp", "", "2nd album from the band"));
		this.daoDisc.save(new Disc(new Long("5"), "I'm With You", "Red Hot Chili Peppers", "rhcp", "", "6th album from the band"));
		this.daoDisc.save(new Disc(new Long("11"), "This Is It", "The Strokes", "the_strokes", "", "1st album from the band"));
		this.daoDisc.save(new Disc(new Long("12"), "Room on Fire", "The Strokes", "the_strokes", "", "2nd album from the band"));
		this.daoDisc.save(new Disc(new Long("13"), "First Impressions Of Earth", "The Strokes", "the_strokes", "", "3rd album from the band"));
		return "success";
	}

}