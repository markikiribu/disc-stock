package main.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DT_DISCS")
public class Disc {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "ALBUM_NAME")
	private String albumName;

	@Column(name = "ARTIST")
	private String artist;

	@Column(name = "ARTIST_CODE")
	private String artistCode;

	@Column(name = "ALBUM_THUMBNAIL")
	private String albumThumbnailUrl;

	@Column(name = "ALBUM_DESCRIPTION")
	private String albumDescription;

	public Disc() {
		super();
	}

	public Disc(Long id, String albumName, String artist, String artistCode, String albumThumbnailUrl, String albumDescription) {
		super();
		this.id = id;
		this.albumName = albumName;
		this.artist = artist;
		this.artistCode = artistCode;
		this.albumThumbnailUrl = albumThumbnailUrl;
		this.albumDescription = albumDescription;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getArtistCode() {
		return artistCode;
	}

	public void setArtistCode(String artistCode) {
		this.artistCode = artistCode;
	}

	public String getAlbumThumbnailUrl() {
		return albumThumbnailUrl;
	}

	public void setAlbumThumbnailUrl(String albumThumbnailUrl) {
		this.albumThumbnailUrl = albumThumbnailUrl;
	}

	public String getAlbumDescription() {
		return albumDescription;
	}

	public void setAlbumDescription(String albumDescription) {
		this.albumDescription = albumDescription;
	}

}