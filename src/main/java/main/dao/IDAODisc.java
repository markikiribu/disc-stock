package main.dao;

import java.util.List;

import main.domain.Disc;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IDAODisc extends JpaRepository<Disc, Long> {

	List<Disc> findAlbumByArtistCode(String artistCode);

	Disc findAlbumByIdAndArtistCode(Long id, String artistCode);
	
	@Query(value="from Disc d where d.id in (:ids)")
	List<Disc> findAlbumsInId(@Param("ids") List<Long> ids);

}