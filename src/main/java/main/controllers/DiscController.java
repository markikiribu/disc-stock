package main.controllers;

import java.util.ArrayList;
import java.util.List;

import main.bl.IDiscographyService;
import main.domain.Disc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/disc-store")
public class DiscController {
	
	@Autowired
	private IDiscographyService discographyService;

	@RequestMapping(method = RequestMethod.GET, path = "/{artist}/{id}", produces = "application/json")
	public Disc albumById(@PathVariable("artist") String artist, @PathVariable("id") Long id) {
		return this.discographyService.getArtistAlbumById(artist, id);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{artist}/all", produces = "application/json")
	public List<Disc> allAlbums(@PathVariable("artist") String artist) {
		return this.discographyService.getArtistAlbums(artist);
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/{artist}/some", produces = "application/json")
	public List<Disc> getSomeAlbums(@PathVariable("artist") String artist) {
		List<Long> ids = new ArrayList<Long>();
		ids.add(new Long("3"));
		ids.add(new Long("4"));
		ids.add(new Long("5"));
		return this.discographyService.getListOfAlbums(ids);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/{artist}/monthAlbum", produces = "application/json")
	public Disc albumOfTheMonth(@PathVariable("artist") String artist) {
		return null;
	}

}