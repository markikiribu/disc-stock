package main.bl;

import java.util.List;

import main.domain.Disc;

public interface IDiscographyService {
	
	List<Disc> getArtistAlbums(String artist);
	
	Disc getArtistAlbumById(String artist, Long albumId);
	
	List<Disc> getListOfAlbums(List<Long> ids);

}