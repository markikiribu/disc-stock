package main.bl.impl;

import java.util.List;

import main.bl.IDiscographyService;
import main.dao.IDAODisc;
import main.domain.Disc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiscographyServiceImpl implements IDiscographyService {
	
	@Autowired
	private IDAODisc daoDisc;

	@Override
	public List<Disc> getArtistAlbums(String artist) {
		return this.daoDisc.findAlbumByArtistCode(artist);
	}

	@Override
	public Disc getArtistAlbumById(String artist, Long albumId) {
		return this.daoDisc.findAlbumByIdAndArtistCode(albumId, artist);
	}

	@Override
	public List<Disc> getListOfAlbums(List<Long> ids) {
		return this.daoDisc.findAlbumsInId(ids);
	}

}
